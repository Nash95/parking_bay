<?php
	require('dbcon.php');

	//Mail++++++Start
	//https://github.com/Synchro/PHPMailer
	require 'PHPMailer-master/src/PHPMailer.php';
	require 'PHPMailer-master/src/Exception.php';
	require 'PHPMailer-master/src/SMTP.php';
	
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	//Mail++++++End

	session_start();
	$email = $_SESSION["email"];
	$model = $_SESSION["model"];
	$vehicle = $_SESSION["vehicle"];
	$status = "RESERVED";
	$plateno = $_SESSION["plateno"];
	$plot = $_SESSION["plot"];
	$account = $_SESSION["account"];
	$street = $_SESSION["street"];
	$from = $_SESSION["from"];
	$to = $_SESSION["to"];
	$charge = $_SESSION["charge"];
	$charge = number_format((float)$charge, 2, '.', '');		
			
	/*CHECK IF RESERVED */
			
	$sql="SELECT * FROM zones WHERE street='$street' and plot='$plot'";
	$result=mysqli_query($conn, $sql);

	// Mysql_num_row is counting table row
	$count=mysqli_num_rows($result);

	if($count==1){
		header('location:error-book.php');
	}
	else
	{

		$query = "INSERT INTO zones (status, email, model, vehicle,street,plot,platenumber,account,charge,d1,d2) VALUES ('$status', '$email', '$model', '$vehicle' , '$street', '$plot', '$plateno', '$account','$charge','$from','$to')";
		$result = mysqli_query($conn, $query);
			
		// mysqli_close($conn);
		
		if($result){
			//Send Mail
			$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
			try {
			//Server settings
			$mail->SMTPDebug = 2;                                 // Enable verbose debug output
			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';  					  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'nigelpanashemakwara@gmail.com';                 // SMTP username
			$mail->Password = '*********';                           // SMTP password
			$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 465;                                    // TCP port to connect to

			//Recipients
			$mail->setFrom('nigelpanashemakwara@gmail.com', 'Nigel');
			$mail ->Subject = "Online Car Reservation Status";
			$mail ->AddAddress($email);
			$mail ->Body = "Lot Successfully booked
				
				Parkade: $street 
				Lot: $plot 
				From: $from
				To: $to
				Charge: $$charge 

			Please call again!     
				
			";
			// $mail ->AddAddress("nigelmak@hotmail.com");
			// $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
			// $mail->addAddress('ellen@example.com');               // Name is optional
			// $mail->addReplyTo('info@example.com', 'Information');
			// $mail->addCC('cc@example.com');
			// $mail->addBCC('bcc@example.com');

			//Attachments
			// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

			// //Content
			// $mail->isHTML(true);                                  // Set email format to HTML
			// $mail->Subject = 'Here is the subject';
			// $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
			// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			$mail->send();
			echo 'Message has been sent';
			} 
			catch (Exception $e) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
			}
			//REDIRECT
			header('location:success-book.php');		
			exit;
		}
	}
?>