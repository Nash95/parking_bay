-- DROP DATABASE parking;

CREATE DATABASE parking;

USE parking;

-- SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `street` text NOT NULL,
  `plot` text NOT NULL,
  `status` text NOT NULL,
  `model` text NOT NULL,
  `vehicle` text NOT NULL,
  `platenumber` text NOT NULL,
  `email` text NOT NULL,
  `account` text NOT NULL,
  `d1` text NOT NULL,
  `d2` text NOT NULL,
  `charge` text NOT NULL,
`id` int(5) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`street`, `plot`, `status`, `model`, `vehicle`, `platenumber`, `email`, `account`, `d1`, `d2`, `charge`, `id`) VALUES
('Rizende STREET', 'PL 002', 'RESERVED', 'MAZDA', 'volvo', 'KAB', 'test@gmail.com', '40204304', '02.11.2019 11:05AM', '02.11.2019 12:05AM', '5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `name` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `phone` text NOT NULL,
  `level` int(11) NOT NULL,
  `status` text NOT NULL,
  `joindate` text NOT NULL,
`id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`name`, `email`, `password`, `phone`, `level`, `status`, `joindate`, `id`) VALUES
('Test', 'test@yahoo.com', '1111', '0778554212', 0, 'Active', '2019/08/30 06:12:29', 4),
('Taps', 'taps@yahoo.com', 'pog', '0772458652', 1, '', '2019/08/30 05:12:29', 3),
('Timboy', 'tintac@gmail.com', 'tim', '0717586489', 0, 'Active', '2019/08/30 04:12:29', 6),
('ruth', 'ruth@gmail.com', 'ruth', '0729667794', 0, 'Active', '2017/08/30 04:12:29', 7),
('victor', 'victor@gmail.com', 'ogesi', '0774525785', 0, 'Active', '2019/08/31 03:12:29', 8),
('admin', 'admin@admin.com', 'admin', '0777777777', 1, 'Active', '2019/08/30 00:12:29', 1),
('user', 'user@user.com', 'user', '0777777777', 0, 'Active', '2019/08/30 00:12:29', 2),
('victor', 'vicky@gmail.com', '1111', '0771771771', 0, 'Active', '2019/07/30 03:12:29', 5),
('ray', 'ray@yahoo.com', 'ray', '0775448452', 0, 'Active', '2019/05/30 03:12:29', 9),
('tino', 'tino@gmail.com', 'tino', '077562114', 0, 'Active', '2019/06/30 03:12:29', 10);

-- --------------------------------------------------------

--
-- Table structure for table `zones`
--

CREATE TABLE IF NOT EXISTS `zones` (
  `street` text NOT NULL,
  `plot` text NOT NULL,
  `status` text NOT NULL,
  `model` text NOT NULL,
  `vehicle` text NOT NULL,
  `platenumber` text NOT NULL,
  `email` text NOT NULL,
  `account` text NOT NULL,
  `d1` text NOT NULL,
  `d2` text NOT NULL,
  `charge` text NOT NULL,
`id` int(3) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `zones`
--

INSERT INTO `zones` (`street`, `plot`, `status`, `model`, `vehicle`, `platenumber`, `email`, `account`, `d1`, `d2`, `charge`, `id`) VALUES
('RIZENDE PARKADE', 'PL 001', 'RESERVED', 'TOYOTA', 'vits', 'DEC-6002', 'test@gmail.com', '7777777', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 3),
('XIMEX PARKADE', 'PL 003', 'RESERVED', 'toyota', 'corolla', 'ADC-2204', 'test@gmail.com', '6666666', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 4),
('RIZENDE PARKADE', 'PL 005', 'RESERVED', 'toyota', 'vits', 'ACR-5899', 'victor@gmail.com', '78889998888844', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 5),
('RIZENDE PARKADE', 'PL 003', 'RESERVED', 'toyota', 'vits', 'ACV-6899', 'pogacm@gmail.cm', '55889999999999', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 2),
('RIZENDE PARKADE', 'PL 009', 'RESERVED', 'toyota', 'vits', 'ACA-8599', 'yomix@yahoo.com', '23456789', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 8),
('XIMEX PARKADE', 'PL 001', 'RESERVED', 'nissan', 'premier', 'DAE-7700', 'data@gmail.com', '1999999999999999999', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 9),
('JOINA BASEMENT', 'PL 004', 'RESERVED', 'premier', 'volvo', 'AED-7700', 'sholo@gmail.com', '19999999999999999', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 10),
('XIMEX PARKADE', 'PL 008', 'RESERVED', 'premier', 'volvo', 'ACT-7007', 'torent@yahoo.com', '777777777777', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 11),
('XIMEX PARKADE', 'PL 009', 'RESERVED', 'premier', 'volvo', 'ABG-7008', 'wow@gmail.com', '7777777777777', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 12),
('JOINA BASEMENT', 'PL 006', 'RESERVED', 'lexus', 'madza', 'AVB-7056', 'kanye@yahoo.com', '785685789', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 1),
('XIMEX PARKADE', 'PL 013', 'RESERVED', 'premier', 'volvo', 'BFG-7048', 'jay@gmail.com', '785685789', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 7),
('JOINA BASEMENT', 'PL 001', 'RESERVED', 'premier', 'volvo', 'DCD-7088', 'bee@gmail.com', '785685789', '02.11.2014 11:05AM', '02.11.2019 12:05AM', '60', 13),
('RIZENDE PARKADE', 'PL 013', 'RESERVED', 'premier', 'volvo', 'AFR-7046', 'pogba@gmail.com', '785685789', '02.11.2019 11:05AM', '02.11.2014 12:05AM', '60', 14),
('XIMEX PARKADE', 'PL 015', 'RESERVED', 'premier', 'volvo', 'AFF-4456', 'martial@gmail.com', '8889999444444', '02.11.2019 11:05AM', '02.11.2014 12:05AM', '60', 6),
('JOINA BASEMENT', 'PL 020', 'RESERVED', 'premier', 'volvo', 'AER-8054', 'lukaku@gmail.com', '77886766666556', '02.11.2019 11:05AM', '02.11.2014 12:05AM', '60', 15);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
 ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zones`
--
ALTER TABLE `zones`
 ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zones`
--
ALTER TABLE `zones`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- INSERT INTO `users` (`name`, `email`, `password`, `phone`, `level`, `status`, `joindate`, `id`) VALUES
-- ('nash', 'nash@gmail.com', '1234', '', 0, '', '', 15);

-- USE parking;
-- 
-- INSERT INTO `users` (`name`, `email`, `password`, `phone`, `level`, `status`, `joindate`, `id`) VALUES
-- ('admin', 'admin@admin.com', '1234', '', 1, '', '', 16);
-- 
-- SELECT * FROM users WHERE email = 'nash@gmail.com' and password = '1234';



